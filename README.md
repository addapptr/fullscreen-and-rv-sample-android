# Fullscreen and RV Sample Android

This app demonstrates fullscreen and rewarded video ads integration using AATKit. 
See also the [fullscreen wiki](https://aatkit.gitbook.io/android-integration/formats/fullscreen-interstitial) and [rewarded video wiki](https://aatkit.gitbook.io/android-integration/formats/rewarded-video) for more instrucions.

---
## Installation
Clone this repository and import into **Android Studio**
```bash
git clone git@bitbucket.org:addapptr/fullscreen-and-rv-sample-android.git
```
---
## Maintainers
Current maintainers:

* [Damian Supera](https://bitbucket.org/damian_s/)
* [Michał Kapuściński](https://bitbucket.org/m_kapuscinski/)